
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', { title: 'Chat demo using socket.io' });
};