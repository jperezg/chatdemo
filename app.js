
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);

var server = http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

// Sockets
var io = require('socket.io').listen(server);

var users = [];
var messages = [];

// Connected socket
io.sockets.on('connection', function(socket) {
	// Welcome
	console.log('User connected');
	socket.emit('hi', messages);

	// Disconnect
	socket.on('disconnect', function() {
		console.log('User disconnected');
		socket.get('username', function(username) {
			var index = users.indexOf(username);
			users.splice(index, 1);
		});
		io.sockets.emit('user offline', {
			count: io.sockets.clients().length,
			users: users
		});
	});

	// New user
	socket.on('new user', function(user) {
		socket.set('username', user.username);
		users.push(user.username);
		io.sockets.emit('user online', {
			count: io.sockets.clients().length,
			users: users
		});
	});

	// message
	socket.on('message', function(msg) {
		messages.push(msg);
		io.sockets.emit('message', msg);
	});
});